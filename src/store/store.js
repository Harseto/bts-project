import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
axios.defaults.baseURL = '18.139.50.74:8080/'

export const store = new Vuex.store({
    state: {
        token: localStorage.getItem('access_token') || null,
    },
    actions: {
        register(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('register', {
                    username: data.username,
                    email: data.email,
                    password: data.password,
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        login(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('/login', {
                    username: data.username,
                    password: data.password
                })
                    .then(response => {
                        const token = response.data.access_token
                        localStorage.setItem('access_token', token)
                        context.commit('retriveToken', token)
                        resolve(response)
                    })
                    .catch(error => {
                        console.log(error)
                        reject(error)
                    })
            })
        },
    },
})