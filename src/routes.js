import App from './App'
import Home from './components/home'
import Login from './components/login'
import Register from './components/register'

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/Login',
        name: 'login',
        component: Login
    },
    {
        path: '/Register',
        name: 'register',
        component: Register
    },
    {
        path: '/checklist',
        name: 'checklist',
        component: App
    },
]

export default routes